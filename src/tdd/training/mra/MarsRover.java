package tdd.training.mra;

import java.util.List;

public class MarsRover {

	private int[][] grid;
	String obstacle1="";
	String obstacle2="";
	String dir="";
	int x = 0;
	int y = 0;
	int maxX;
	int maxY;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) {
		
		grid = new int[planetX][planetY];
		
		for(int i=0; i<grid.length; i++) {
			for(int j=0; j<grid.length; j++) {
				grid[i][j]=0;
			}
		}
		
		// 0 = free; 1=Rover; 2=Obstacle
		
		obstacle1=planetObstacles.get(0);
		grid[getposition(obstacle1,0)][getposition(obstacle1,1)]=2; 
		
		obstacle2=planetObstacles.get(1);
		grid[getposition(obstacle2,0)][getposition(obstacle2,1)]=2; 
		
		grid[0][0]=1;
		dir="N";
		
		maxX=planetX-1;
		maxY=planetY-1;
			
		
	}

	private int getposition(String obs, int i) {
		String intValue1=obs.replaceAll("[^0-9]","");
		
		return Integer.parseInt(intValue1.charAt(i)+"");
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) {
		boolean o=false;
		
		if(grid[x][y] == 2) {
			o=true;
		}
		return o;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) {
			
		
		
		if(commandString.length()<=1) {
			
			if(commandString == "") {
				return getposition();
			}else {
				
				singleCommand(commandString);
				
			}
		}else {
			char[] ch=commandString.toCharArray();
			
			for(int i=0; i<commandString.length(); i++) {
				
				if(commandString == "") {
					return getposition();
				}else {
					switch (ch[i]) {	
					
					case 'r':
						 turnRight();
						 break;
						
					case 'l':
						 turnLeft();
						 break;
						
					case 'f':
						 forward();
						 break;

					case 'b':
						 backward();
						 break;
					default:
						break;
					
						}
				}
				
					
			}
		}
		
		
			
		return "("+x+","+y+","+dir+")";
	}

	private String singleCommand(String commandString) {
		
		switch (commandString) {	
		
		case "r":
			 turnRight();
			 return "("+x+","+y+","+dir+")";
			
		case "l":
			 turnLeft();
			 return "("+x+","+y+","+dir+")";
			
		case "f":
			 forward();
			 return "("+x+","+y+","+dir+")";

		case "b":
			 backward();
			 return "("+x+","+y+","+dir+")";
			
		default:
			break;
		
			}
		return null;
		
	}

	private void backward() {
		
		switch(dir) {
		case "N": y--;
			if(y<0) {
				y=maxY;
			}
		break;
		case "S": y++;break;
		case "W": x++;break;
		case "E": x--;break;
		default: break;
		}
	}

	private void forward() {
		
		switch(dir) {
		case "N": y++;break;
		case "S": y--;break;
		case "W": x--;break;
		case "E": x++;break;
		default: break;
		}
		
	}

	private void turnLeft() {
		
		switch(dir) {
		case "N": dir="W";break;
		case "S": dir="E";break;
		case "W": dir="S";break;
		case "E": dir="N";break;
		default: break;
		}
		
	}
	
private void turnRight() {
		
		switch(dir) {
		case "N": dir="E";break;
		case "S": dir="W";break;
		case "W": dir="N";break;
		case "E": dir="S";break;
		default: break;
		}
	}

	private String getposition() {
		
		for(int i=0; i<grid.length; i++) {
			for(int j=0; j<grid.length; j++) {
				
				if(grid[i][j]==1) {
					x=i;
					y=j;
				}
			}
		}
		return "("+x+","+y+","+dir+")";
	}

	

}
