package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void obstacle1ShouldBeFound() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(0, 1);
		
		assertTrue(obstacle);
	}
	
	@Test
	public void obstacle2ShouldBeFound() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(4, 4, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(2, 3);
		
		assertTrue(obstacle);
	}
	
	@Test
	public void positionShouldBeReturned() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,N)", returnString);
	}
	
	@Test
	public void roverShouldBeTurnRight() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "r";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,E)", returnString);
	}
	
	@Test
	public void roverShouldBeTurnLeft() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "l";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,0,W)", returnString);
	}

	@Test
	public void roverShouldMoveForward() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,1,N)", returnString);
	}
	
	@Test
	public void roverShouldMoveForwardTwoTimes() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		 returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,2,N)", returnString);
	}

	@Test
	public void roverShouldMoveBackward() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		
		commandString = "b";
		returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,N)", returnString);
	}
	
	@Test
	public void roverShouldAcceptCombinedMoving() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(3, 3, planetObstacles);
		
		
		String commandString = "ffrff";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(2,2,E)", returnString);
	}
	
	@Test
	public void roverShouldBeGoOnOppositeEdge() {
		
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,1)");
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		
		String commandString = "b";
		String returnString = rover.executeCommand(commandString);
		
		assertEquals("(0,9,N)", returnString);
	}
}
